import pandas as pd
import os

from utils import t0_to_string

count = 0

for t in pd.date_range('2021-04-03 00:00:00+0000', '2022-09-28 21:44:45+0000', freq='15min', tz='UTC'):
  if not os.path.exists(f'/PATH/TO/samples/{t0_to_string(t)}.pickle'):
    print(t)
    count += 1

print("Total missing:", count)
