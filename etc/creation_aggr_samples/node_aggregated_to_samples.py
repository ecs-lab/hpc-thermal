from multiprocessing import Pool
import os
import pandas as pd
import time

from utils import print_now

BASE_PATH_ = os.path.join('/PATH/TO')
AGGREG_FOLDER_  = os.path.join(BASE_PATH_, 'aggregated_inst')
SAMPLES_FOLDER_ = os.path.join(BASE_PATH_, 'samples_inst')

timestamps = pd.date_range('2021-04-03 00:00:00+0000',
                           '2022-09-28 21:44:45+0000', freq='15min', tz='UTC')
# timestamps = pd.read_csv('missing_timestamps.txt', header=None, parse_dates=[0])[0]
parallel = 64

os.makedirs(SAMPLES_FOLDER_, exist_ok=True)
dfs = {}
for node in range(980):
  print(node)
  path = os.path.join(AGGREG_FOLDER_, f'{node}.parquet')
  df = pd.read_parquet(path).set_index('timestamp')
  dfs[node] = df
  print_now()

def save_pickle(t):
  start = time.time()
  rows = [df.loc[t].rename(str(n)) for n, df in dfs.items() if t in df.index]
  sample = pd.concat(rows, axis=1).T.reindex([str(n) for n in range(980)])
  path = os.path.join(SAMPLES_FOLDER_,
                      f'{t.replace(tzinfo=None)}.pickle'.replace(' ', '_'))
  sample.to_pickle(path)
  del sample
  print(f"Saved to {path} in {time.time()-start} seconds")

with Pool(parallel) as pool:
  pool.map(save_pickle, timestamps)
print("Bye!")
