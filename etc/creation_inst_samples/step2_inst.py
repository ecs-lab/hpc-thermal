from exadata.parquet_dataset.query_tool import M100DataClient
import os
import time
import logging
import multiprocessing as mp
import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq

NPROC=64
dataset_path = '/PATH/TO/dataset'
step1_path = '/PATH/TO/step1_inst_results_temp'
results_path = '/PATH/TO/aggregated_inst'
pa.set_io_thread_count(NPROC)
pa.set_cpu_count(NPROC)

def worker(queue, ipmi_metrics):
    logger = logging.getLogger(__name__)
    while True:
        queue_tuple = queue.get()
        if queue_tuple is None:
            break
            
        s = time.time()
        node, state_node = queue_tuple
        first_done = False

        for i, metric in enumerate(ipmi_metrics):
            node_metric_path = os.path.join(step1_path, f'node={node}/metric={metric}/')
            if os.path.exists(node_metric_path):
                #s = time.time()
                cols = ['timestamp', metric]
                
                if first_done:
                    df_o = pq.read_table(node_metric_path, columns=cols).to_pandas()
                    df_o.set_index('timestamp', inplace=True)
                    df = pd.concat((df, df_o), axis=1)
                else:
                    df = pq.read_table(node_metric_path, columns=cols).to_pandas()
                    df.set_index('timestamp', inplace=True)
                    first_done = True

        # left join with labels
        if first_done and len(df)>0:
            state_node = state_node.set_index('timestamp').drop('node', axis=1)
            state_node.columns = ['state']
            df = pd.concat((df, state_node), axis=1)
            df.reset_index(inplace=True)

            #df.to_parquet(os.path.join('./aggregated_data_per_node',f'{node}.parquet'), index=False)
            df = pa.Table.from_pandas(df)
            pq.write_table(df,
                           os.path.join(results_path, f'{node}.parquet'),
                           compression='zstd',
                           compression_level=19)
        
        logging.info(f'[{node}] Done in {time.time()-s}')


if __name__ == '__main__':

    # logger
    logging.basicConfig(level=logging.INFO,
                        format='[%(asctime)s] %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    logger = logging.getLogger(__name__)

    s0 = time.time()
    client = M100DataClient(dataset_path)
 
    ipmi_metrics = sorted(client.metrics_per_plugin['ipmi'])
    # ipmi_metrics.remove('0_0') # test metric that still has to be removed 
    ipmi_nodes = [x.split('=')[1] for x in os.listdir(step1_path)]
    
    s1=time.time()
    labels = client.query('state',
                          description='batchs::client::state',
                          columns=['timestamp', 'value', 'node'])

    labels = labels[labels.node.isin(ipmi_nodes)]
    labels.node = labels.node.cat.remove_unused_categories()
    logging.info(time.time()-s1)

    # splitting labels by node
    s2 = time.time()
    label_nodes = [x for x in labels.groupby('node')]
    logging.info(time.time()-s2)
    del labels
    
    os.makedirs(results_path, exist_ok=True)

    # multiprocessing
    queue = mp.Queue()
    #iolock = mp.Lock()

    s3 = time.time()
    while len(label_nodes) > 0:
        queue.put(label_nodes.pop())
    logging.info(time.time()-s3)

    # terminate workers
    for _ in range(NPROC):
        queue.put(None)

    s4 = time.time()
    pool = mp.Pool(NPROC, initializer=worker, initargs=(queue, ipmi_metrics))
    logging.info(time.time()-s4)

    # waiting
    pool.close()
    pool.join()

    logging.info(f'Done all in {time.time()-s0}')
