import os
import pandas as pd
import torch
from torch.utils.data import DataLoader
from typing import Self

from torch_dataset_graph import TorchGraphTrainingDataset
from utils import BASE_PATH_, timestamp_to_pickle_name


class SimpleData(dict):
  """Simple data structure mimicking `torch_geometric` `Data` objects

  This is a dictionary class whose members can be accessed by dot notation,
  i.e. `obj.x` instead of `obj['x']`. It is only intended to contain `x` and
  `y` members with `torch.Tensor`s as values. The class also has a method to
  perform device conversion of these tensors.
  """
  __getattr__ = dict.get
  __setattr__ = dict.__setitem__
  __delattr__ = dict.__delitem__

  def __str__(self) -> str:
    return (f"SimpleData(x={self.x.shape}, y={self.y.shape}, "
            f"metrics_names=[{len(self.metrics_names)}])")

  def to(self, device: str) -> Self:
    self.x = self.x.to(device)
    self.y = self.y.to(device)
    return self


class TorchNodeTrainingDataset(TorchGraphTrainingDataset):
  """PyTorch dataset class for single-node M100 training data.

  It is similar to its parent class, but only used to recover data for one
  single M100 node. Accordingly, it accepts a `node` constructor parameter and
  has modified versions of the getter methods, which return `SimpleData`
  objects.
  """
  def __init__(self, torch_folder: str, tstart_train: str, tstop_train: str,
                     eligible_log: str, node: int):
    super().__init__(torch_folder, tstart_train, tstop_train, eligible_log)
    self.node = node

  def load_torch_sample(self, t: pd.Timestamp) -> SimpleData:
    """Return `SimpleData` object loading it from a Pickle file"""
    file = os.path.join(self.torch_folder, timestamp_to_pickle_name(t))
    data_obj = torch.load(file)
    ret = SimpleData(x=data_obj.x[self.node, :], y=data_obj.y[self.node],
                     metrics_names=data_obj.metrics_names)
    return ret

  def __getitem__(self, idx: int) -> SimpleData:
    """
    Recover one sample by converting `idx` to the `idx`-th valid timestamp
    """
    t_idx = self.training_ts[idx]
    return self.load_torch_sample(t_idx)


if __name__ == '__main__':
  node = 85
  batch_size = 10
  target = 'pcie_t1'
  torch_fold = os.path.join(BASE_PATH_, f'torch_y={target}')
  eligible_log = os.path.join('resources', f'eligible_y={target}.txt')
  tstart = '2021-04-03 00:15:00'
  ttest =  '2021-05-03 00:15:00'
  dataset = TorchNodeTrainingDataset(torch_fold, tstart, ttest, eligible_log,
                                     node)
  print("Number of samples:", len(dataset))
  loader = DataLoader(dataset, batch_size=batch_size, shuffle=True)
  print(len(loader), "batches of (maximum) size", batch_size)
  for batch in loader:
    print(batch)
  print("Bye!")
