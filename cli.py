import argparse

from train_model import train_model


"""Command-Line Interface for train_model.py

Besides collecting command-line arguments and calling the training function, it
only converts the comma-separated `neurons` string to a list of integers.
All arguments are always mandatory except the last three: `--edges-file` and
`--weights-file` must only be provided when training a graph model, while
`--node` must only be provided when training a single-node model.
"""

par = argparse.ArgumentParser(description="Launch and log training of "
                              "single-node or graph neural network models")
par.add_argument("--network-type", help="name of network class to use")
par.add_argument("--neurons", nargs='*', default=[], type=int,
                              help="list of layer sizes")
par.add_argument("--num-epochs", type=int, help="number of training epochs")
par.add_argument("--batch-size", type=int, help="training batch size")
par.add_argument("--rng-seed", type=int, help="starting RNG seed")
par.add_argument("--target-metric", help="name of regression target metric")
par.add_argument("--samples-folder", help="folder with Torch sample files")
par.add_argument("--eligible-log",
                  help="path of file with training-eligible timestamps")
par.add_argument("--lvp-metric", help="metric to use as Last-Value Predictor")
par.add_argument("--train-loss", help="loss to minimize during training")
par.add_argument("--eval-loss", help="loss to use for test-set evaluation")
par.add_argument("--node", type=int,
                 help="number of compute node (for single-node models only)")
par.add_argument("--time-intervals-file", help="CSV file with time-consistent "
                 "time intervals (for dynamic models only)")
args = par.parse_args()

# Call training script
train_model( **vars(args) )
