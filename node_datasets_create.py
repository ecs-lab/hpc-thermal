import numpy as np
import os
import pandas as pd

from torch_dataset_graph import TorchGraphTrainingDataset
from utils import BASE_PATH_, TSTART, TTEST, get_time_instants


"""Creates datasets for individual nodes, assembling Torch samples"""


def write(node: int, strg: str, mode: str = 'a') -> None:
  """Write given `strg` to `node`'s CSV file"""
  with open(os.path.join(output_folder, f'node{node}.csv'), mode) as f:
    f.write(strg+"\n")


def write_row(row: np.array, timestamp: pd.Timestamp) -> None:
  """Write contents of `row` in the appropriate way, by calling `write()`"""
  strg = ','.join([str(timestamp)] + row[1:].tolist())
  write(row[0], strg)


def write_node_datasets(torch_folder: str, eligible_log: str) -> None:  
  dataset = TorchGraphTrainingDataset(torch_folder, TSTART, TTEST,
                                      eligible_log)
  print("Initialized dataset")
  df_columns = ['pcie_avg_t-1', 'total_power_avg_t-1', 'ambient_avg_t-1',
                'total_power_avg_t0', 'pcie_t0']
  all_nodes = list(range(980))
  all_nodes_np = np.array(all_nodes).reshape(-1, 1)
  # Write headers with column names
  header = ','.join(['time'] + df_columns + ['target'])
  for node in all_nodes:
    write(node, header, 'w')

  # Loop over timestamps
  for t in get_time_instants():
    if not dataset.eligible(t):
      print(t, "not eligible, skipped")
      continue

    print(t)
    # Read data as strings
    data_t = dataset.load_torch_sample(t)
    # Prepare array in which each row is: node#, features..., target
    nodes_x_y = np.hstack((all_nodes_np,
                           data_t.x.numpy().astype(str),
                           data_t.y.numpy().reshape(-1, 1).astype(str)))
    # Apply writing function row-wise
    np.apply_along_axis(write_row, 1, nodes_x_y, timestamp=t)
    del data_t, nodes_x_y


if __name__ == '__main__':
  target = 'pcie_t1_delta'
  torch_folder = os.path.join(BASE_PATH_, f'torch_minimal_y={target}')
  eligible_log = os.path.join('resources', f'eligible_y={target}.txt')
  output_folder = os.path.join(BASE_PATH_, 'nodes_csv_minimal')
  write_node_datasets(torch_folder, eligible_log)
  print("Bye!")
