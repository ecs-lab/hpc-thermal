import torch
from torch.nn import Linear
from torch_geometric.data import Data
from torch_geometric.nn import GCNConv


class ReducedGCNStaticPredictionModel(torch.nn.Module):
  """Network with linear and GCN layers for predictions on time-constant graphs

  The goal is to reduce the complexity of data via a linear layer before
  feeding it to a convolutional layer. `num_inputs` is the network input size,
  but the `neurons` parameter goes unused.
  """
  is_static_model = True

  def __init__(self, num_inputs: int, neurons: list[int] = []):
    super().__init__()
    self.linear_layer = Linear(num_inputs, 1)
    self.output_layer = GCNConv(1, 1)

  def forward(self, data: Data) -> torch.Tensor:
    x = data.x
    x = self.linear_layer(x)
    x = x.relu()
    x = self.output_layer(x, data.edge_index, data.edge_weight)
    return x


if __name__ == '__main__':
  model = ReducedGCNStaticPredictionModel(num_inputs=200)
  print(model)
