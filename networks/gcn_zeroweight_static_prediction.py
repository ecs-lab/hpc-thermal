import torch
from torch_geometric.data import Data

from networks.gcn_static_prediction import GCNStaticPredictionModel


class GCNZeroWeightStaticPredictionModel(GCNStaticPredictionModel):
  """
  Same as GCNStaticPredictionModel, but it always sets weights of the graph
  edges to zero during forward passes.
  """
  def forward(self, data: Data) -> torch.Tensor:
    x = data.x
    edge_weight = torch.zeros_like(data.edge_weight)
    for layer in self.hidden_layers:
      x = layer(x, data.edge_index, edge_weight)
      x = x.relu()
    x = self.output_layer(x, data.edge_index, edge_weight)
    return x
