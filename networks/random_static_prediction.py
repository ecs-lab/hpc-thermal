import numpy as np
import os
import torch
from torch_geometric.data import Data
from torch_geometric.nn import GCNConv


class GCNRandomGraphStaticPredictionModel(torch.nn.Module):
  """GCN network for predictions on time-constant graphs

  The constructor accepts `neurons`, a list of layer sizes, in which the i-th
  entry is the output dimension of the i-th layer. `num_inputs` is the network
  input size. A layer with output size 1 is automatically added as the last
  output layer.
  """
  is_static_model = True

  def __init__(self, num_inputs: int, neurons: list[int]):
    super().__init__()
    if len(neurons) == 0:
      neurons = [num_inputs]
      self.hidden_layers = []
    else:
      hidden_list = [GCNConv(num_inputs, neurons[0])]
      for i in range(len(neurons)-1):
        hidden_list.append(GCNConv(neurons[i], neurons[i+1]))
      self.hidden_layers = torch.nn.ModuleList(hidden_list)
    self.output_layer = GCNConv(neurons[-1], 1)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    self.edge_index = torch.tensor(np.loadtxt(os.path.join('resources',
                              'edges_random_rng24.csv')).T,
                              dtype=torch.long).to(device)
    self.edge_weight = torch.tensor(np.loadtxt(os.path.join('resources',
                              'weights_random_rng24.csv')),
                              dtype=torch.float).to(device)

  def forward(self, data: Data) -> torch.Tensor:
    x = data.x
    for layer in self.hidden_layers:
      x = layer(x, self.edge_index, self.edge_weight)
      x = x.relu()
    x = self.output_layer(x, self.edge_index, self.edge_weight)
    return x


if __name__ == '__main__':
  model = GCNRandomGraphStaticPredictionModel(num_inputs=200, neurons=[8])
  print(model)
