import torch
from torch_geometric.data import Data
from torch_geometric.nn import GCNConv


class GCNStaticPredictionModel(torch.nn.Module):
  """GCN network for predictions on time-constant graphs

  The constructor accepts `neurons`, a list of layer sizes, in which the i-th
  entry is the output dimension of the i-th layer. `num_inputs` is the network
  input size. A layer with output size 1 is automatically added as the last
  output layer.
  """
  is_static_model = True

  def __init__(self, num_inputs: int, neurons: list[int]):
    super().__init__()
    if len(neurons) == 0:
      neurons = [num_inputs]
      self.hidden_layers = []
    else:
      hidden_list = [GCNConv(num_inputs, neurons[0])]
      for i in range(len(neurons)-1):
        hidden_list.append(GCNConv(neurons[i], neurons[i+1]))
      self.hidden_layers = torch.nn.ModuleList(hidden_list)
    self.output_layer = GCNConv(neurons[-1], 1)

  def forward(self, data: Data) -> torch.Tensor:
    x = data.x
    for layer in self.hidden_layers:
      x = layer(x, data.edge_index, data.edge_weight)
      x = x.relu()
    x = self.output_layer(x, data.edge_index, data.edge_weight)
    return x


if __name__ == '__main__':
  model = GCNStaticPredictionModel(num_inputs=200, neurons=[8, 5, 3])
  print(model)
