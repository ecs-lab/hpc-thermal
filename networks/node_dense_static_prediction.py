import torch
from torch.nn import Linear


class NodeDenseStaticPredictionModel(torch.nn.Module):
  """Dense neural network for predictions on a single node

  The constructor accepts `neurons`, a list of layer sizes, in which the i-th
  entry is the output dimension of the i-th layer. `num_inputs` is the network
  input size. A layer with output size 1 is automatically added as the last
  output layer.
  Note that the `forward()` method accepts the custom object
  `torch_dataset_node.SimpleData` (see `torch_dataset_node.py`), which behaves
  similarly to a `torch_geometric` `Data` object and of which only the `x`
  member is accessed.
  """
  is_static_model = True

  def __init__(self, num_inputs: int, neurons: list[int]):
    super().__init__()
    if len(neurons) == 0:
      neurons = [num_inputs]
      self.hidden_layers = []
    else:
      hidden_list = [Linear(num_inputs, neurons[0])]
      for i in range(len(neurons)-1):
        hidden_list.append(Linear(neurons[i], neurons[i+1]))
      self.hidden_layers = torch.nn.ModuleList(hidden_list)
    self.output_layer = Linear(neurons[-1], 1)

  def forward(self, data: 'SimpleData') -> torch.Tensor:
    x = data.x
    for layer in self.hidden_layers:
      x = layer(x)
      x = x.relu()
    x = self.output_layer(x)
    return x


if __name__ == '__main__':
  model = NodeDenseStaticPredictionModel(num_inputs=200, neurons=[8, 5, 3])
  print(model)
