import torch
from torch_geometric.data import Data

from networks.gcn_static_prediction import GCNStaticPredictionModel


class GCNLearnableWeightStaticPredictionModel(GCNStaticPredictionModel):
  """
  Same as GCNStaticPredictionModel, but the weights of the graph edges also are
  learnable parameters.
  """
  def __init__(self, num_inputs: int, neurons: list[int]):
    super().__init__(num_inputs, neurons)
    # TODO: fix hardcoded num_edges
    num_edges = 4782
    self.edge_weight = torch.nn.Parameter(torch.ones(num_edges))

  def forward(self, data: Data) -> torch.Tensor:
    x = data.x
    for layer in self.hidden_layers:
      try:
        x = layer(x, data.edge_index, self.edge_weight.sigmoid())
      except IndexError:
        raise IndexError("This network only supports batch size 1")
      x = x.relu()
    x = self.output_layer(x, data.edge_index, self.edge_weight.sigmoid())
    return x
