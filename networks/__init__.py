import importlib
import os

# This imports all classes from all files in this folder,
# under the 'networks' namespace
module_files = [f[:-3] for f in os.listdir(os.path.dirname(__file__))
                       if f.endswith('.py') and f != '__init__.py']
for module_name in module_files:
  module = importlib.import_module(f'.{module_name}', package=__name__)
  globals().update({name: getattr(module, name) for name in module.__dict__
                                                if not name.startswith('_')})
