import torch
from torch.nn import Linear
from torch_geometric.data import Data
from torch_geometric_temporal.nn.recurrent import DCRNN


class DCRNNDynamicPredictionModel(torch.nn.Module):
  """DCRNN network for predictions on time-changing graphs

  The constructor accepts `neurons`, a list of layer sizes, in which the i-th
  entry is the output dimension of the i-th layer. `num_inputs` is the network
  input size. A layer with output size 1 is automatically added as the last
  output layer.
  """
  is_static_model = False

  def __init__(self, num_inputs: int, neurons: list[int]):
    super().__init__()
    if len(neurons) > 1:
      raise ValueError("DCRNNDynamicPredictionModel only supports one layer")
    filter_size = 2
    self.recurrent_layer = DCRNN(num_inputs, neurons[0], K=filter_size)
    self.output_layer = Linear(neurons[0], 1)

  def forward(self, data: Data) -> torch.Tensor:
    h = self.recurrent_layer(data.x, data.edge_index, data.edge_weight)
    h = h.relu()
    h = self.output_layer(h)
    return h


if __name__ == '__main__':
  model = DCRNNDynamicPredictionModel(num_inputs=200, neurons=[100])
  print(model)
