import logging
import os
import pandas as pd
from torch.utils.data import DataLoader as NodeDataLoader
from torch_geometric.loader import DataLoader as GraphDataLoader

from torch_dataset_graph import TorchGraphTrainingDataset
from torch_dataset_node import TorchNodeTrainingDataset
from utils import BASE_PATH_


class TorchTimeMetaLoader:
  """Class to create and loop on data loaders associated to time intervals

  This class is needed to properly handle data when training time-changing
  models, both single-node and graph. In particular, each time-consistent
  interval (see `time_intervals.py`) must undergo training at once, and
  separately from each other. For this reason, this class creates a list of
  data loaders which the training function can loop on.
  `dataset_type` (usually equal to `TorchGraphTrainingDataset` or
  `TorchNodeTrainingDataset`) and `loader_type` (usually `GraphDataLoader` or
  `NodeDataLoader`) are class names, needed to initialize the correct dataset
  and loader classes. `intervals_file` is the path to the CSV file containing
  the aforementioned time intervals. These will be read, and one loader will be
  initialized for each of them, until intervals exceed the training datetime
  limit `tstop_train`. The last interval will be trimmed so as to end at such
  limit.
  The remaining constructor arguments are needed for the initialization of
  dataset and loader files.
  """
  def __init__(self, dataset_type: type, loader_type: type, torch_folder: str,
               tstop_train: str, intervals_file: str, batch_size: int,
               **dataset_kwargs):
    self.logger = logging.getLogger(__name__)
    self.time_intervals = pd.read_csv(intervals_file)
    int_rows = self.time_intervals.shape[0]
    self.logger.info("Initializing metaloader")
    self.logger.debug(f"Original intervals file contains {int_rows} intervals")
    tlast = pd.to_datetime(tstop_train, utc=True)
    stop = False

    # Initialize loaders
    self.loaders = []
    for i in range(int_rows):
      # Get interval
      interval_start, interval_stop = self.time_intervals.loc[i]
      # If the interval goes beyond the training limit, we trim it down and
      # conclude the creation of new loaders
      if pd.to_datetime(interval_stop, utc=True) > tlast:
        self.logger.debug(f"Trimming last interval from {interval_stop} "
                          f"to {tstop_train}")
        interval_stop = tlast
        stop = True
      self.logger.debug(f"Initializing loader with data from {interval_start} "
                        f"to {interval_stop}")
      dataset = dataset_type(torch_folder, interval_start, interval_stop,
                             **dataset_kwargs)
      loader = loader_type(dataset, batch_size=batch_size, shuffle=False)
      self.loaders.append(loader)
      if stop:
        break

    self.logger.info(f"Initialization of {len(self.loaders)} loaders "
                      "complete")

  def __len__(self) -> int:
    return len(self.loaders)

  def __iter__(self) -> list['loader_type']:
    yield from self.loaders



if __name__ == '__main__':
  logging.basicConfig(level=logging.DEBUG)
  logger = logging.getLogger(__name__)
  target = 'pcie_t1'
  torch_fold = os.path.join(BASE_PATH_, f'torch_y={target}')
  eligible_log = os.path.join('resources', f'eligible_y={target}.txt')
  intervals_file = os.path.join('resources', 'time_intervals_2023-08-30.csv')
  batch_size = 20
  tstop_train = '2021-05-04 12:00'
  # metaloader = TorchTimeMetaLoader(TorchNodeTrainingDataset, NodeDataLoader,
  #                 torch_fold, tstop_train, intervals_file, batch_size,
  #                 eligible_log=eligible_log, node=85)
  metaloader = TorchTimeMetaLoader(TorchGraphTrainingDataset, GraphDataLoader,
                  torch_fold, tstop_train, intervals_file, batch_size,
                  eligible_log=eligible_log)
  logger.info(f"Initialized metaloader with {len(metaloader)} time chunks")
  for i, chunk_loader in enumerate(metaloader):
    num_batches = len(chunk_loader)
    logger.info(f"Chunk {i} has {num_batches} batches")
    for j, batch in enumerate(chunk_loader):
      logger.info(f"  Batch {j} of {num_batches}: {batch}")
