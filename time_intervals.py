from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import os
import pandas as pd

from utils import get_list_from_txt


def get_time_consistent_intervals(eligible_log: str) \
                                  -> tuple[pd.DataFrame, Figure]:
  """Create list of consecutive time-consistent data intervals

  The given file `eligible_log` contains a list of eligible timestamps. This
  function divides that list into time-consistent intervals, i.e. ones which
  have consecutive timestamps every 15 minutes without holes. It also creates
  and returns a plot to visualize these intervals.
  """
  delta = pd.Timedelta(minutes=15)
  col_names = ['start', 'stop']
  intervals = pd.DataFrame(columns=col_names)
  times = get_list_from_txt(eligible_log)
  interval_start = None
  for i in range(len(times)-1):
    t0 = times[i]
    t1 = times[i+1]
    if interval_start is None:
      interval_start = t0
    elif t1 - t0 != delta:
      newrow = pd.Series((interval_start, t0), index=col_names)
      intervals = intervals.append(newrow, ignore_index=True)
      interval_start = None

  # Close last interval
  if interval_start is not None:
    newrow = pd.Series((interval_start, times[-1]), index=col_names)
    intervals = intervals.append(newrow, ignore_index=True)

  # Display intervals
  fig, ax = plt.subplots(figsize=(30, 2))
  for _, (tstart, tstop) in intervals.iterrows():
    ax.hlines(y=0, xmin=tstart, xmax=tstop, lw=5)
    redline = ax.axvline(x=tstop, ymin=0.3, ymax=0.7, ls='--', color='red')

  ax.margins(x=0.001)
  ax.set_xticks(pd.date_range(intervals.iloc[0, 0],
                              intervals.iloc[-1, -1], freq='10D'))
  ax.set_yticks([])
  ax.set_title(f"Number of intervals: {intervals.shape[0]}")
  ax.legend(handles=[redline], labels=['interval ends'])
  fig.autofmt_xdate()

  return intervals, fig


if __name__ == '__main__':
  file = os.path.join('resources', 'eligible_2023-08-10.txt')
  df_int, fig_int = get_time_consistent_intervals(file)
  df_int.to_csv('time_intervals.csv', index=False)
  fig_int.savefig('time_intervals.png', bbox_inches='tight', dpi=300)
