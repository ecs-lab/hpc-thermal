import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
from sklearn.base import RegressorMixin
from sklearn.linear_model import Ridge
from sklearn.metrics import mean_squared_error as MSE
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

from utils import BASE_PATH_


def get_node_mse(dataset_file: str, model: RegressorMixin,
                 features: list[str] | None = None) -> np.ndarray:
  """
  Train single-node `model` with data from `dataset_file` and return the MSE

  This also prints relevant model information in the process. If `features` is
  None, all dataset features will be used, otherwise only the given features
  will.
  """
  # Read and split data
  df = pd.read_csv(dataset_file)
  y = df['target']
  X = df.drop('target', axis=1)
  if features is not None:
    X = X[features]
  X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2,
                                                            shuffle=False)
  # Normalize data
  scaler = StandardScaler()
  X_train = scaler.fit_transform(X_train)
  X_test = scaler.transform(X_test)

  # Train model
  model = model.fit(X_train, y_train)
  # Display model
  if hasattr(model, 'coef_'):
    print("Coefficients:")
    for col, coef in zip(X.columns, model.coef_):
      print(col, "=", round(coef, 4), "with range",
                                     f"{X[col].min()} ~ {X[col].max()}")
  yhat_test = model.predict(X_test)
  mse_val = MSE(y_test, yhat_test)
  print("MSE =", mse_val, "\n")

  del df, X, y, X_train, X_test, y_train, y_test
  return np.hstack(([mse_val], model.coef_))


def get_all_nodes_mse(nodes: list[int], model: RegressorMixin,
                      features: list[str] | None = None) -> pd.DataFrame:
  """Train a separate single-node model for each of the given `nodes`

  This function returns a `pandas.DataFrame` containing the MSE for each node.
  See `get_node_mse()` for an explanation on the `features` argument.
  """
  print("Training models")
  df_mse = pd.DataFrame(index=nodes, columns=['mse'] + features)
  for node in nodes:    
    file = os.path.join('/PATH/TO', f'node{node}.csv')
    print(f"For node {node}:")
    df_mse.loc[node] = get_node_mse(file, model, features)
  return df_mse


def get_node_train_prediction(dataset_file: str, model: RegressorMixin) \
                              -> pd.DataFrame:
  """
  Return training predictions for `model` with data from `dataset_file`

  Returns model predictions on training data. All dataset features are used.
  """
  # Read and split data
  print(f"Training with {os.path.split(dataset_file)[-1]}")
  df = pd.read_csv(dataset_file, index_col='time')
  y = df['target']
  X = df.drop('target', axis=1)
  X_train, _, y_train, _ = train_test_split(X, y, test_size=0.2, shuffle=False)
  # Train model and save training predictions
  model = model.fit(X_train, y_train)
  yhat = model.predict(X)
  df_yhat = pd.DataFrame(yhat, index=X.index)

  del df, X, y, X_train, y_train, yhat
  return df_yhat


def get_all_nodes_train_predictions(nodes: list[int], model: RegressorMixin) \
                                    -> None:
  pred_list = [get_node_train_prediction(os.path.join(BASE_PATH_,
                                         'nodes_csv_minimal', f'node{n}.csv'),
                                        model) for n in nodes]
  df_pred_all = pd.concat(pred_list, axis=1)
  df_pred_all.columns = nodes
  return df_pred_all


def plot_results(df: pd.DataFrame, model_name: str, plot_file: str) -> None:
  """Plot MSEs in decreasing order and save plot"""
  fig, ax = plt.subplots()
  df_sorted = df.sort_values('mse', ascending=False)
  df_sorted.index = df_sorted.index.astype(str)
  mse_min = np.floor(df_sorted.iloc[-1, 0])
  mse_max = np.ceil(df_sorted.iloc[0, 0]) + 0.01
  mse_avg = round(df_sorted.mean().item(), 4)
  ax.plot(df_sorted, marker='.', label='MSEs', zorder=2)
  ax.axhline(mse_avg, c='red', ls='--', label='average MSE', zorder=0)
  ax.set_xticks([])
  ax.set_ylim(mse_min - 0.01, mse_max + 0.01)
  ax.set_yticks(np.arange(mse_min, mse_max, 1), minor=False)
  ax.grid(axis='y', which='major', alpha=1)
  ax.set_yticks(np.arange(mse_min, mse_max, 0.5), minor=True)
  ax.grid(axis='y', which='minor', alpha=0.5)
  ax.set_title(f"Sorted MSEs of single-node {model_name} models")
  ax.legend()

  # Save plot
  fig.savefig(plot_file, bbox_inches='tight', dpi=300)
  print("Saved plot to", plot_file)


if __name__ == '__main__':
  nodes = range(980)
  model = Ridge()
  model_name = type(model).__name__
  features = ['pcie_avg_t-1', 'total_power_avg_t-1', 'ambient_avg_t-1',
              'pcie_t0']

  # Load results, or obtain them by training the models
  dataset_file = f'nodes_norm_{len(features)}.csv'
  if os.path.exists(dataset_file):
    print("Loading", dataset_file)
    df = pd.read_csv(dataset_file, index_col='node')
  else:
    df = get_all_nodes_mse(nodes, model, features)
    df.to_csv(dataset_file, index_label='node')
    # df = get_all_nodes_train_predictions(nodes, model)
    # df.to_csv(dataset_file, index_label='time')
    print(df)
    print("Saved results to", dataset_file)

  # Plot results
  # plot_results(df, model_name, dataset_file.replace('.csv', '.png'))
  
  print("Bye!")
