import logging
import numpy as np
import os
import time
import torch
import torch.nn.functional as torch_F
from torch.utils.data import DataLoader as NodeDataLoader
from torch_geometric.loader import DataLoader as GraphDataLoader
import torchmetrics

import networks
from torch_dataset_graph import TorchGraphTrainingDataset
from torch_metaloader import TorchTimeMetaLoader
from torch_dataset_node import TorchNodeTrainingDataset
from utils import TSTART, TTEST, TSTOP, error_diagnostics, get_time_instants, \
                                        print_now


def train_model(network_type: str, neurons: list[int], num_epochs: int,
    batch_size: int, rng_seed: int, target_metric: str, samples_folder: str,
    eligible_log: str, lvp_metric: str, train_loss: str, eval_loss: str,
    node: int | None, time_intervals_file: str | None) -> None:
  """Train single-node or graph model

  If the former case, you must pass an appropriate `node` argument; oherwise,
  if you pass None, the latter case is assumed.
  This function initializes a network model from the `networks` folder whose
  name is `network_type`. (See the files in such folder for details about
  `neurons`.) `eligible_log` is a file containing all timestamps whose samples
  can be used during training or testing; a sample with a timestamp not
  included in this file will simply be skipped. You can also choose the name
  of the training and evaluation losses, drawn from the `torch.nn.functional`
  and `torchmetrics` modules respectively. `lvp_metric` indicates the name of
  the metric that will act as the Last-Value Predictor (LVP) for a baseline
  comparison, or is a constant float value for the LVP.
  After initializing the model and other classes, it performs batch training
  with shuffled data, which is read from Pickle files from `samples_folder`.
  It then evaluates the model by sequentially providing the samples in the test
  set. All relevant training and evaluation information is saved into two CSV
  log files, whose names summarize most of the training settings. Please refer
  to `utils.error_diagnostics()` for more details about the logged information.
  """
  # Initialize logger
  logging.basicConfig(level=logging.INFO)
  logger = logging.getLogger(__name__)

  if node is not None:
    logger.info(f"Training single-node model for node {node}")
    single_node = True
  else:
    logger.info("Training graph model")
    single_node = False

  # Set up random seeds and logging
  np.random.seed(rng_seed)
  torch.manual_seed(rng_seed)
  # Create experiment signature
  curr_time = time.strftime('%Y-%m-%d_%H:%M', time.localtime())
  exper_signature = f'{network_type}_{curr_time}_'
  if single_node:
    exper_signature += f'node{node}_'
  exper_signature += (f'layer{len(neurons)+1}_'
                      f'epoch{num_epochs}_batch{batch_size}_'
                      f'target-{target_metric}_rng{rng_seed}_'
                      f'losses-{train_loss}-{eval_loss}')
  train_log = f'errors_train_{exper_signature}.csv'
  test_log = f'errors_test_{exper_signature}.csv'

  is_static_model = getattr(networks, network_type).is_static_model
  if not is_static_model and time_intervals_file is None:
    raise ValueError(f"{network_type} dynamic model needs time_intervals_file")
  kind = "static" if is_static_model else "dynamic"
  logger.info(f"Initializing data objects for {kind} model "
              f"with target metric {target_metric}...")
  if not os.path.exists(samples_folder):
    raise OSError(f"Non-existent Torch folder {samples_folder}")
  logger.info(f"Using Torch samples from {samples_folder}")
  if single_node:
    dataset_class = TorchNodeTrainingDataset
    dataset_kwargs = dict(eligible_log=eligible_log, node=node)
    loader_class = NodeDataLoader
  else:
    dataset_class = TorchGraphTrainingDataset
    dataset_kwargs = dict(eligible_log=eligible_log)
    loader_class = GraphDataLoader
  dataset = dataset_class(samples_folder, TSTART, TTEST, **dataset_kwargs)
  logger.debug(f"Created {dataset_class} object")
  if is_static_model:
    training_loader = loader_class(dataset, batch_size=batch_size,
                                            shuffle=True)
    logger.debug(f"Created {loader_class} object")
  else:
    metaloader = TorchTimeMetaLoader(dataset_class, loader_class,
                    samples_folder, TTEST, time_intervals_file, batch_size,
                    **dataset_kwargs)
    batch_count = 0
    logger.debug(f"Created metaloader with {loader_class} class")
  logger.info("Done")

  logger.info("Initializing model...")
  num_inputs = len(dataset[0].metrics_names)
  device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
  model = getattr(networks, network_type)(num_inputs, neurons).to(device)
  optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
  train_loss_func = getattr(torch_F, train_loss)
  eval_loss_func = getattr(torchmetrics, eval_loss)().to(device)
  logger.info(f"Succefully initialized the following {kind} model:\n{model}")
  print_now(logger)

  test_log_headers = ['time'] + list(error_diagnostics(None, None, None, None,
                                                       None).keys())
  with open(train_log, 'a') as f1:
    f1.write('epoch,batch,loss\n')
  with open(test_log, 'a') as f2:
    f2.write(','.join(test_log_headers) + '\n')

  logger.info("Starting training")
  training_start = time.time()
  model.train()
  for epoch in range(num_epochs):
    logger.info(f"Starting epoch {epoch} of {num_epochs}")
    epoch_start = time.time()
    # Training loop for one epoch of the static case
    if is_static_model:
      for i, batch in enumerate(training_loader):
        batch = batch.to(device)
        logger.info(f"Loaded batch {i} of {len(training_loader)}: {batch}")
        optimizer.zero_grad()
        out = model(batch)
        loss = train_loss_func(torch.flatten(out), batch.y)
        loss.backward()
        optimizer.step()
        with open(train_log, 'a') as f1:
          f1.write(f'{epoch},{i},{loss.item()}\n')
        logger.info(f"Training loss on batch = {loss.item()}")
        logger.debug(f"Batch {i} complete")
    # Training loop for one epoch of the dynamic case
    else:
      for i, chunk_loader in enumerate(metaloader):
        num_batches = len(chunk_loader)
        logger.info(f"Loaded time-consistent chunk {i} of {len(metaloader)}, "
                    f"with {num_batches} batches")
        for j, batch in enumerate(chunk_loader):
          batch = batch.to(device)
          logger.info(f"Loaded batch {j} of {num_batches} "
                      f"(number {batch_count} overall): {batch}")
          optimizer.zero_grad()
          out = model(batch)
          loss = train_loss_func(torch.flatten(out), batch.y)
          loss.backward()
          optimizer.step()
          with open(train_log, 'a') as f1:
            f1.write(f'{epoch},{batch_count},{loss.item()}\n')
          logger.info(f"Training loss on batch = {loss.item()}")
          batch_count += 1
        logger.info(f"Chunk {i} complete")
      # At the end of the dynamic training loop
      batch_count = 0

    # At the end of the epoch, for both static and dynamic training
    logger.info(f"Epoch {epoch} complete in {time.time()-epoch_start} "
                 "seconds")
    print_now(logger)

  # Epoch training loop has ended
  logger.info("Starting evaluation")
  model.eval()
  # Samples for test set, i.e. TTEST ~ TSTOP
  for t in get_time_instants(TTEST, TSTOP):
    if not dataset.eligible(t):
      logger.info(f"{t} (test) not eligible; skipping")
    else:
      logger.info(f"{t} (test)")
      data = dataset.load_torch_sample(t).to(device)
      # Compute prediction, error, and diagnostics
      y_pred = model(data)
      err_dict = error_diagnostics(train_loss_func, eval_loss_func, data,
                                   y_pred, lvp_metric)
      err_dict = {'time': t} | err_dict
      err_dict['ytrue'] = '/'.join(map(str, err_dict['ytrue']))
      err_dict['ypred'] = '/'.join(map(str, err_dict['ypred']))
      with open(test_log, 'a') as f2:
        f2.write(','.join(map(str, err_dict.values())) + '\n')

    if t.day == 1 and t.hour == 0 and t.minute == 0:
      print_now(logger)

  # weights_file = f'weights_{exper_signature}.pt'
  # torch.save(model.state_dict(), weights_file)
  # logger.info(f"Model parameters saved to {weights_file}")
  logger.info("Training completed in "
             f"{(time.time()-training_start)/60} minutes")
  logger.info(f"Experiment signature: {exper_signature}")
  logger.info("Bye!")
