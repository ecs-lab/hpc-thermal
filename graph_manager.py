from collections.abc import Callable
import logging
import numpy as np
import os
import pandas as pd
import torch
from torch_geometric.data import Data

from room import Marconi100Room
from utils import JOBS_FILE_, SAMPLES_AGGR_FOLDER_, SAMPLES_INST_FOLDER_, \
                  get_list_from_txt, get_time_instants, print_now, \
                  timestamp_to_pickle_name


class GraphDataManager:
  """
  Class that manages data to feed a graph model.

  This class uses specific prepared samples coming from an elaboration of the
  node-aggregated dataset, for the purposes of training a graph model. Such
  model aims at predicting the value of `target_metric` at a future timestamp,
  given data from previous timestamps. Data is collected into samples, each
  representing a snapshot of the node-level metrics of the M100 dataset at a
  specific timestamp. These timestamps are 15 minutes apart, and samples
  collect either pointwise instantaneous values of metrics at that time, or
  their values in aggregated form (avg, min, max, std) over the next 15-minute
  period.
  This class can load these data samples, both instanteaneous and aggregated,
  and combine them in several ways (see `load_metric_selection_file()`). It can
  modify the data, fill in the NaNs with meaningful values, and prepare a
  `torch_geometric` `Data` object that can be readily fed to a graph model. It
  also defines a criterion for eligibility to filter which samples are used for
  model training. Note that this class only uses prepared samples from file,
  and never calls the data-extracting ExaData query tool during its operations.
  """
  def __init__(self, target_metric: str,
               edge_index:  torch.Tensor | None = None,
               edge_weight: torch.Tensor | None = None,
               metric_selection_file: str | None = None):
    self.logger = logging.getLogger(__name__)
    self.target_metric = target_metric
    self.edge_index = edge_index
    self.edge_weight = edge_weight
    self.room = Marconi100Room()
    self.t_delta = pd.Timedelta(minutes=15)
    self.load_metric_selection_file(metric_selection_file)
    self.jobs_df = None


  def load_metric_selection_file(self, file: str | None = None) -> None:
    """
    Initialize members for selection of metrics in different timestamps

    In particular, it will load a .csv file to the `metric_sel_info` member.
    Each row contains: metric name, data access form ('aggr'egated or
    'inst'ant), aggregate type/suffix (only for the 'aggr' case), timestamp
    index. Each row corresponds to a metric in a specific form and at a
    specific point in time. For instance, row "pcie,aggr,avg,-1" means that we
    request the aggregated value of the 'pcie' metric with 'avg' aggregation at
    time t_-1. (If you want to use a certain metric with multiple combinations
    of form/suffix/time, you have to use one row for each combination.)
    This method also loads the `metric_sel_combinations` member, which is used
    internally for efficient data extraction (see `load_time_mixed_sample()`).
    """
    if file is None:
      self.metric_sel_info = None
      self.metric_sel_combinations = None
      return

    self.metric_sel_info = pd.read_csv(file)
    self.metric_sel_combinations = {'aggr': [], 'inst': []}
    for _, (metric, form, agg, time_) in self.metric_sel_info.iterrows():
      if time_ not in self.metric_sel_combinations[form]:
        self.metric_sel_combinations[form].append(time_)
    self.logger.debug("Initialized metric selection combinations: "
                     f"{self.metric_sel_combinations}")


  def load_raw_sample(self, t: pd.Timestamp, aggregated: bool = True) \
                      -> pd.DataFrame:
    """
    Get 15-minute-window sample from prepared dataset

    If `aggregated` is `True`, the aggregated sample containing 15-minute
    average, min, max, and standard deviation will be loaded, otherwise the
    sample with pointwise instant values will be.
    """
    if aggregated:
      agg_str = "aggregated"
      samples_folder = SAMPLES_AGGR_FOLDER_
    else:
      agg_str = "pointwise"
      samples_folder = SAMPLES_INST_FOLDER_
    self.logger.info(f"Loading {agg_str} raw sample at {t}")
    file = os.path.join(samples_folder, timestamp_to_pickle_name(t))
    df = pd.read_pickle(file).drop('state', axis=1).astype(float)
    self.logger.debug("Raw sample is ready")
    return df


  def load_time_mixed_sample(self, t0: pd.Timestamp) -> pd.DataFrame:
    """
    Load sample from several files, starting from timestamp `t0` as the center

    Loads samples from files, selects metrics according to the
    `metric_sel_info` member (see `load_metric_selection_file`), and returns a
    joined DataFrame combining all these sample slices. Thus, the returned
    `DataFrame` can have both aggregate and instantaneous metrics, from time
    `t0` and from other previous and subsequent timestamps.
    """
    if self.metric_sel_info is None or self.metric_sel_combinations is None:
      raise RuntimeError("Metric selection file was not initialized")

    # Perform loops on form strings ('aggr' or 'inst') and time jumps (-1, 0,
    # 1...), for which all possible (requested) combinations are present in the
    # `metric_sel_combinations` member. This complicated loop is necessary to
    # load only the samples from which data is actually extracted. Moreover,
    # we only want to load each sample once, and extract all requested metrics
    # from that sample at once. All of this is important because we want to
    # minimize the number of times we load a file, since it is a very expensive
    # operation.
    dfs = []
    for form, jumps in self.metric_sel_combinations.items():
      aggregated = True if form == 'aggr' else False
      for jump in jumps:
        t = t0 + jump * self.t_delta
        self.logger.debug(f"Loading with aggregated={aggregated} and "
                          f"jump={jump}")
        df_sample = self.load_raw_sample(t, aggregated)
        # Create boolean masks for metrics
        form_mask = self.metric_sel_info['form'] == form
        jump_mask = self.metric_sel_info['time'] == jump
        if aggregated:
          # Only select metrics with both matching `form` AND `jump`
          cols_aggr = self.metric_sel_info.loc[form_mask & jump_mask,
                                               ['metric', 'agg']]
          # Join to get aggregated metric names (e.g. pcie, max -> pcie_max)
          columns = cols_aggr.agg('_'.join, axis=1)
        else:
          # Only select metrics with both matching `form` AND `jump`
          columns = self.metric_sel_info.loc[form_mask & jump_mask, 'metric']
        self.logger.debug(f"Columns to select (with suffix '_t{jump}'): "
                          f"{columns.tolist()}")
        # Select and rename columns to avoid clashes with other `df`
        df = df_sample[columns].add_suffix(f"_t{jump}")
        dfs.append(df)

    # Join all collected metrics
    return pd.concat(dfs, axis=1)


  def reduce_coretemp_time_mixed_metrics(self, df: pd.DataFrame) \
                                         -> pd.DataFrame:
    """
    Squash together time-mixed coretemp. metrics and return updated `DataFrame`

    Samples contain, among other things, the metrics pX_coreY_tempZ_tT, where
    X = {0, 1}, Y = {0, ..., 23}, Z = {_avg, _min, _max, _std, ''}, T integer.
    (Z is '' in cases where the metrics are instantaneous and not aggregated.)
    For each combination of Z and T, this method averages over X and Y to get
    a new metric coretempZ_tT, which replaces the old metrics in the returned
    `DataFrame`.
    """
    if self.metric_sel_info is None or self.metric_sel_combinations is None:
      raise RuntimeError("Metric selection file was not initialized")
    orig_metrics = get_list_from_txt(os.path.join('resources',
                                        'metrics_nodes_core_temp.txt'))

    # ASSUMPTION: all original core temperature metrics included in
    # `metric_sel_info` (i.e. all elements of `orig_metrics`) have the same
    # settings as each other
    is_coretemp_mask = self.metric_sel_info['metric'].isin(orig_metrics)
    if is_coretemp_mask.sum() == 0:
      # No core temperature metric present
      return df
    coretemp_df = self.metric_sel_info.loc[is_coretemp_mask]
    coretemp_times = coretemp_df['time'].value_counts().index
    coretemp_form = coretemp_df.iloc[0]['form']
    suffixes = ('_' + coretemp_df['agg']).value_counts().index \
               if coretemp_form == 'aggr' else ('',)
    for t in coretemp_times:
      for suff in suffixes:
        all_suff_metrics = [f'{m}{suff}_t{t}' for m in orig_metrics]
        present_suff_metrics = [m for m in df.columns if m in all_suff_metrics]
        newmet = df[present_suff_metrics].agg(np.nanmean, axis=1)
        newmet_name = f'coretemp{suff}_t{t}'
        df[newmet_name] = newmet
        df.drop(present_suff_metrics, axis=1, inplace=True)
        self.logger.debug(f"Condensed {newmet_name} from "
                          f"{present_suff_metrics}")

    return df


  def add_jobs_column(self, df: pd.DataFrame, t0: pd.Timestamp) \
                      -> pd.DataFrame:
    """
    Add bool column on whether nodes are running a job(s) at `t0`

    This column is created by accessing a pre-prepared Pickle file which stores
    information for all columns and timestamps. This file is buffered as a
    class member to avoid loading times when calling this method multiple
    times. The method addes the column to the input `df` and returns the
    updated version.
    """
    if self.jobs_df is None:
      self.jobs_df = pd.read_pickle(JOBS_FILE_).astype(float)
    job_col = self.jobs_df[t0]
    job_col.name = 'jobs_running'
    return pd.concat((df, job_col), axis=1)


  def get_time_mixed_df(self, t0: pd.Timestamp) -> pd.DataFrame:
    """
    Utility returning `DataFrame` with data from different timestamps and forms

    In particular, this method collects data from different timestamps, in
    either aggregated and/or instantaneous form, as defined in the input metric
    selection file (see `load_metric_selection_file`). It then calls other
    methods before returning a data sample ready for graph model training.
    """
    df = self.load_time_mixed_sample(t0)
    df = self.reduce_coretemp_time_mixed_metrics(df)
    df = df.rename(columns={self.target_metric: 'target'})
    return df


  def get_time_mixed_delta_df(self, t0: pd.Timestamp) -> pd.DataFrame:
    """
    Utility returning `DataFrame` with delta metric as target column

    Like `get_time_mixed_df()`, this method collects data from different
    timestamps and forms. However, it also creates the target column as a
    difference between the target metric and its counterpart at time `t0`,
    provided that the target metric end with `_delta` (this method will raise
    an error otherwise). For instance, if the target metric is `pcie_t3_delta`,
    the target column will be computed as `pcie_t3` minus `pcie_t0`. `pcie_t3`
    will also be removed from the sample in the process.
    """
    if not self.target_metric.endswith('_delta'):
      raise ValueError("Metric name must be <METRIC>_t<HORIZON>_delta")
    df = self.load_time_mixed_sample(t0)
    df = self.reduce_coretemp_time_mixed_metrics(df)
    # Get involved metrics to compute the delta
    orig_metric = self.target_metric.rstrip('_delta')
    split = orig_metric.split('_t')
    split[-1] = '0'
    t0_metric = '_t'.join(split)
    # Add delta metric
    df['target'] = df[orig_metric] - df[t0_metric]
    df.drop(orig_metric, axis=1, inplace=True)
    self.logger.info("Created target column from difference between "
                    f"{orig_metric} and {t0_metric}")
    return df



  def get_time_mixed_horizon_df(self, t0: pd.Timestamp,
                                      aggregator: Callable) -> pd.DataFrame:
    """
    Return `DataFrame` by also creating a time-aggregated target column

    Like `get_time_mixed_df()`, this method collects data from different
    timestamps and forms. However, it also creates the target column, by
    aggregating columns from different time instants, based on the name of
    the initialized target metric. The columns will be aggregated via the
    `aggregator` function, which must accept an array-like object and return
    one value. For instance, if it is 'pcie_max_t3', this method will extract
    all columns named 'pcie_max' from samples at time t_0, t_1, t_2, and use
    `aggregator` to compute the aggregate over t_0, t_1, t_2 for each node.
    Note that when using aggregated metrics (such as 'pcie_max'), this is the
    equivalent of computing the aggregate metric from time t0 to time t3.
    """
    df = self.load_time_mixed_sample(t0)
    df = self.reduce_coretemp_time_mixed_metrics(df)
    # Get metric name and horizon from target metric, e.g. 'pcie_max' and 3
    base_metric = self.target_metric.split('_t')[0]
    horizon = int(self.target_metric.split('_t')[-1])

    self.logger.info(f"Creating aggregation of {base_metric} "
                     f"with horizon {horizon}")
    future_columns = []
    # Loop over time steps within the horizon
    for step in range(horizon):
      metric_name = f'{base_metric}_t{step}'  # e.g. 'pcie_max_t1'
      # Only load raw sample if needed, i.e. if base metric isn't already there
      if metric_name in df.columns:
        newcol = df[metric_name]
      else:
        newcol = self.load_raw_sample(t0 + step*self.t_delta,
                                      aggregated=True)[base_metric]
      future_columns.append(newcol)

    # Aggregate columns from the different timestamps into one via `aggregator`
    df['target'] = pd.concat(future_columns, axis=1).agg(aggregator, axis=1)
    self.logger.info(f"{base_metric} aggregation created")
    return df


  def get_nan_info(self, df: pd.DataFrame) -> dict[str: object]:
    """
    Return dictionary with information on NaNs contained in `df`

    Specifically, letting y be the 'target' column and X everything else:
    * 'empty': bool indicating if `df` is empty or not (so that the other
               entries can be ignored if i is empty)
    * 'nanx': number of NaNs in the X matrix (excluding the target metric)
    * 'nany': number of NaNs in the y vector of the target metric
    * 'nrows': number of rows (nodes) which contain one or more NaNs
    * 'ncols': number of columns (metrics) which contain one or more NaNs
    * 'rows_names': list of rows (nodes) which contain one or more NaNs
    * 'cols_names': list of columns (metrics) which contain one or more NaNs
    """
    if df.empty:
      self.logger.debug("Empty DataFrame in get_nan_info()")
      return {'empty': True, 'nanx': 0, 'nany': 0, 'nrows': 0, 'ncols': 0,
              'rows_names': [], 'cols_names': []}

    # Split into X and y and compute NaN masks
    y = df['target']
    X = df.drop(columns='target')
    X_isna = X.isna().to_numpy()
    y_isna = y.isna()

    # Count NaNs
    nan_x = X_isna.sum().item()
    nan_y = y_isna.sum().item()
    rows_idxs = X_isna.any(axis=1)
    rows_names = X.index[rows_idxs.tolist()].tolist()
    nrows = rows_idxs.sum().item()
    cols_idxs = X_isna.any(axis=0)
    cols_names = X.columns[cols_idxs.tolist()].tolist()
    ncols = cols_idxs.sum().item()

    info = {'empty': False, 'nanx': nan_x, 'nany': nan_y, 'nrows': nrows,
            'ncols': ncols, 'rows_names': rows_names, 'cols_names': cols_names}
    self.logger.debug(f"NaN info: {info}")
    return info


  def check_eligible(self, data: pd.DataFrame) -> bool:
    """Assess whether the given DataFrame is eligible for training"""
    info = self.get_nan_info(data)
    # !!!
    return not (info['empty'] or info['nany'] > 1 or info['nrows'] >= 10)


  def neighbor_average_fill(self, df: pd.DataFrame) -> pd.DataFrame:
    """
    Fill NaNs in `df` with average of neighbor values

    In particular, if the value of metric m of node n is NaN, it will be
    replaced with a weighted average of the values of m of the spatial
    neighbors of n (see `Marconi100room` class). The weights are proportional
    to the inverse of the distance between n and the neiighbor. If the value
    of a neighbor is also NaN, that neighbor is ignored in the computation of
    the average.
    """
    self.logger.info("Filling NaNs...")
    vals = df.to_numpy()  # for efficiency
    # Loop over rows and columns of X matrix
    for node in range(vals.shape[0]):
      for col in range(vals.shape[1]):
        # If value is NaN, fill it
        if np.isnan(vals[node, col]):
          self.logger.debug(f"df[{node},{col}] is NaN")
          # get neighboring nodes
          neigh_dist = self.room.get_neighbors_and_distances(node)
          # compute weighted average excluding NaNs
          values = []
          weights = []
          for n, d in neigh_dist.items():
            val_n = vals[n, col]
            if np.isnan(val_n):
              self.logger.debug(f"Neighbor {n} is NaN, skipping")
            else:
              values.append(val_n)
              weights.append(1 / d)
              self.logger.debug(f"Neighbor {n} has value {val_n} and "
                                f"weight {round(1 / d, 3)}")
          # set value in data
          df.iloc[node, col] = np.average(values, weights=weights)
          self.logger.debug(f"Filled with {df.iloc[node, col]}")

    self.logger.info("Filling complete")
    return df


  def prepare_torch_data(self, df: pd.DataFrame) -> Data:
    """Separate X and y, and build Data object with tensor members"""
    if self.edge_index is None or self.edge_weight is None:
      raise RuntimeError("Edge index/weight member(s) were not initialized")
    self.logger.debug("Preparing data in Torch format")
    y_df = df['target']
    X_df = df.drop(columns='target')
    self.logger.debug("Initializing Torch tensors")
    X = torch.tensor(X_df.to_numpy(), dtype=torch.float)
    y = torch.tensor(y_df.to_numpy(), dtype=torch.float)
    self.logger.debug("Initializing data container and validating")
    data = Data(x=X, y=y, metrics_names=list(X_df.columns),
                edge_index=self.edge_index, edge_weight=self.edge_weight)
    data.validate(raise_on_error=True)
    self.logger.debug("Done")
    return data


if __name__ == '__main__':
  logging.basicConfig(level=logging.INFO)
  met_file = os.path.join('resources', 'metrics_nodes_mixed.csv')
  manager = GraphDataManager('pcie_t1_delta', metric_selection_file=met_file)
  for t in get_time_instants():
    print("Extracting time-mixed data at", t)
    data = manager.get_time_mixed_delta_df(t)
    print(data)
    print("Done")
    print_now()
