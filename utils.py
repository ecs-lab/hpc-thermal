import logging
import os
import pandas as pd
import time
import torch
from torch.nn.modules.loss import _Loss as Loss
from torch_geometric.data import Data
from torchmetrics.metric import Metric


BASE_PATH_ = os.path.join('/PATH/TO')
JOBS_FILE_ = os.path.join(BASE_PATH_, 'jobs_running.pickle')
SAMPLES_AGGR_FOLDER_ = os.path.join(BASE_PATH_, 'samples_aggr')
SAMPLES_INST_FOLDER_ = os.path.join(BASE_PATH_, 'samples_inst')

TSTART = '2021-04-03 00:15:00'
TSTOP  = '2022-09-28 21:00:01'
TTEST  = '2022-07-05 12:45:00'


def get_list_from_txt(file: str) -> list[str]:
  """
  Read list of metrics/dates/etc from text file, ignoring '#' comments

  This method reads a file with a single entry on each row. Dates will be read
  as `pandas.Timestamp`s objects if possible. Lines starting with '#' will be
  ignored.
  """
  return pd.read_csv(file, header=None, comment='#', parse_dates=[0]) \
           .squeeze().tolist()


def get_metrics_list_from_selection_file(file: str) -> list[str]:
  """Get full list of metrics computing combinations of aggregations/times"""
  df = pd.read_csv(file)
  metrics = set()
  coretemp_metrics = get_list_from_txt(os.path.join('resources',
                                          'metrics_nodes_core_temp.txt'))
  for _, (orig_metric, form, times_str) in df.iterrows():
    times_list = times_str.split()
    # Handle core temperature metrics
    if orig_metric in coretemp_metrics:
      orig_metric = 'coretemp'
    if form == 'inst':
      new_metrics = {f'{orig_metric}_t{t}' for t in times_list}
    elif form == 'aggr':
      new_metrics = {f'{orig_metric}_{s}_t{t}'
                     for s in ('avg', 'min', 'max', 'std') for t in times_list}
    else:
      raise ValueError("Values in 'form' column must be 'aggr' or 'inst'")
    metrics.update(new_metrics)
  return list(metrics)


def get_time_instants(start: str = TSTART, stop: str = TSTOP) \
                       -> list[pd.Timestamp]:
  """"Get time instants from `start` to `stop` with a 15-minute frequency"""
  return pd.date_range(start, stop, freq='15min', tz='UTC').tolist()


def print_now(logger: logging.Logger | None = None) -> None:
  """Print current wall clock time"""
  currtime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
  output = f"Current time: {currtime}\n-----\n"
  if logger is None:
    print(output, flush=True)
  else:
    logger.info(output)


def timestamp_to_pickle_name(t: pd.Timestamp) -> str:
  """Turn timestamp `t` into the name of a Pickle file"""
  return str(t.replace(tzinfo=None)).replace(' ', '_') + '.pickle'


def string_to_timestamp(strg: str) -> pd.Timestamp:
  """Recover timestamp from interval string"""
  t0 = pd.to_datetime(strg.split('__')[0].replace('_', ' '), utc=True)
  return t0


def get_metadata_df(file: str) -> pd.DataFrame:
  """
  Load pandas DataFrame containing metadata information from log file

  It assumes a 'time' column with values in the following format:
  'YYYY-MM-DD_HH:MM:SS__YYYY-MM-DD_HH:MM:SS'. This column will be trimmed to
  the first date and used as index.
  """
  df = pd.read_csv(file, sep=';')
  # time column is 'tstart__tstop': let's set the index as just tstart
  df['time'] = df['time'].apply(string_to_timestamp)
  df.set_index('time', inplace=True)
  return df


def error_diagnostics(train_loss: Loss | None, eval_loss: Metric | None,
                      data: Data, y_pred: torch.Tensor, lvp_metric: str) \
                      -> dict[str: float]:
  """
  Return dictionary with information to assess prediction quality on test set

  It receives a data sample and the values predicted by the model, computes
  relevant values, and returns a dictionary filled with the appropriate keys
  and values (or None values if any of `train_loss` or `eval_loss` is None).
  Specifically, the dictionary contains:
  * `ytrue`, the original target values
  * `ypred`, the values predicted by the model
  * `train_loss`, the computed value of the training loss on this sample
  * `eval_loss`, the computed value of the evaluation loss on this sample
  * `train_loss_lvp`, the computed value of the training loss on the Last-Value
    Predictor (`lvp_metric` is either the metric name representing the LVP, or
    a constant float value for it)
  * `eval_loss`, the computed value of the evaluation loss on the Last-Value
    Predictor.
  """
  ret = dict.fromkeys(['train_loss', 'eval_loss', 'train_loss_lvp',
                       'eval_loss_lvp', 'ytrue', 'ypred'], None)
  if train_loss is None or eval_loss is None:
    return ret
  # Errors on prediction
  y_flat = torch.flatten(data.y.reshape(1, -1))
  y_pred_flat = torch.flatten(y_pred.reshape(1, -1))
  ret['train_loss'] = train_loss(y_pred_flat, y_flat).item()
  ret['eval_loss'] = eval_loss(y_pred_flat, y_flat).item()
  # Errors of last-value prediction
  if lvp_metric in data.metrics_names:
    target_idx = data.metrics_names.index(lvp_metric)
    dim = data.x.shape[-1]
    last_value = torch.flatten(data.x.reshape(-1, dim)[:, target_idx])
  else:
    const_vector = len(y_flat)*[float(lvp_metric)]
    last_value = torch.Tensor(const_vector).to(y_flat.get_device())
  ret['train_loss_lvp'] = train_loss(last_value, y_flat).item()
  ret['eval_loss_lvp'] = eval_loss(last_value, y_flat).item()
  # True vs predicted values
  ret['ytrue'] = y_flat.detach().cpu().numpy()
  ret['ypred'] = y_pred_flat.detach().cpu().numpy()
  return ret
