import os
import pandas as pd
import torch
from torch.utils.data import Dataset
from torch_geometric.data import Data
from torch_geometric.loader import DataLoader

from utils import BASE_PATH_, get_list_from_txt, get_time_instants, \
                  timestamp_to_pickle_name


class TorchGraphTrainingDataset(Dataset):
  """PyTorch dataset class for M100 training data.

  This simple class is needed to perform batch training of a graph model, both
  static and time-dependent. It loads the list of eligible training timestamps
  by intersecting the given training period [`tstart_train`, `tstop_train`]
  with the eligible timestamps from a log file. It then links integer indexes
  to timestamps using a list of eligible timestamps, and returns data samples
  by reading them on the fly from `torch_folder`.
  Like all `Dataset` classes, it can be given to a `DataLoader` object which in
  turn can be looped on in order to get training batches.
  """
  def __init__(self, torch_folder: str, tstart_train: str, tstop_train: str,
                     eligible_log: str):
    self.torch_folder = torch_folder
    # Get list of valid training timestamps
    self.all_ts = get_time_instants(tstart_train, tstop_train)
    self.eligible_ts = get_list_from_txt(eligible_log)
    # Compute intersection between `all_ts` and `eligible_ts`
    self.training_ts = [t for t in self.all_ts if t in self.eligible_ts]

  def __len__(self) -> int:
    return len(self.training_ts)

  def __str__(self) -> str:
    tstart = self.all_ts[0].strftime("%Y-%m-%d %H:%M")
    tstop = self.all_ts[-1].strftime("%Y-%m-%d %H:%M")
    return (f"{type(self).__name__} class for {tstart} ~ {tstop} "
            f"({len(self)} eligible samples)")

  def eligible(self, t: pd.Timestamp) -> bool:
    """Return whether `t` is in the list of eligible timestamps"""
    return (t in self.eligible_ts)

  def load_torch_sample(self, t: pd.Timestamp) -> Data:
    """Return `torch_geometric` `Data` object loading it from a Pickle file"""
    file = os.path.join(self.torch_folder, timestamp_to_pickle_name(t))
    return torch.load(file)

  def __getitem__(self, idx: int) -> Data:
    """
    Recover one sample by converting `idx` to the `idx`-th valid timestamp

    This method is needed to make batch sampling work. It extracts the sample
    corresponding to the timestamp and returns a prepared `torch_geometric`
    `Data` object.
    """
    t_idx = self.training_ts[idx]
    return self.load_torch_sample(t_idx)


if __name__ == '__main__':
  batch_size = 10
  target = 'pcie_t1'
  torch_fold = os.path.join(BASE_PATH_, f'torch_y={target}')
  eligible_log = os.path.join('resources', f'eligible_y={target}.txt')
  tstart = '2021-04-03 00:15:00'
  ttest =  '2021-05-03 00:15:00'
  dataset = TorchGraphTrainingDataset(torch_fold, tstart, ttest, eligible_log)
  print(dataset)
  loader = DataLoader(dataset, batch_size=batch_size, shuffle=True)
  print(len(loader), "batches of (maximum) size", batch_size)
  for batch in loader:
    print(batch)
  print("Bye!")
