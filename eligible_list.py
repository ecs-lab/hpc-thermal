import os

from utils import BASE_PATH_, get_time_instants, timestamp_to_pickle_name


def create_eligible_log(torch_folder: str, log: str) -> None:
  """Produce a text file containing dates associated to eligible Torch samples

  Samples are read from `torch_folder`. Whether a sample is eligible or not is
  deduced by its size. If it is larger than the size of the empty
  `torch.Tensor`, i.e. 719 bytes, this means that it has been deemed eligible
  by the `save_data.py` script, therefore it is added to the file. Otherwise,
  it is ignored.
  """
  for t in get_time_instants():
    torch_file = os.path.join(torch_folder, timestamp_to_pickle_name(t))
    if os.path.getsize(torch_file) > 719:
      print(t, "eligible")
      with open(log, 'a') as f:
        f.write(f"{t}\n")


if __name__ == '__main__':
  target_metric = 'pcie_t1_delta'
  torch_fold = os.path.join(BASE_PATH_, f'torch_y={target_metric}')
  log = f'eligible_y={target_metric}.txt'
  create_eligible_log(torch_fold, log)
