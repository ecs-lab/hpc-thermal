import logging
from multiprocessing import Pool
import numpy as np
import os
import pandas as pd
import torch
import time

from graph_manager import GraphDataManager
from utils import BASE_PATH_, get_time_instants, print_now, \
                  timestamp_to_pickle_name


"""Utility script that uses `GraphDataManager` to save data into Torch samples

The `parallelism` level of this operation can be indicated in the __main__.
"""


def save_pickle(t: pd.Timestamp) -> None:
  """Save sample associated to timestamp `t` as a Pickle file

  If the sample is not eligible, an empty `torch.Tensor` will be saved.
  """
  print("Extracting time-mixed data at", t)
  if target_metric.endswith('_delta'):
    df = manager.get_time_mixed_delta_df(t)
  elif 'aggregator' in globals():
    df = manager.get_time_mixed_horizon_df(t, aggregator)
  else:
    df = manager.get_time_mixed_df(t)

  # Prepare Torch data if eligible
  if manager.check_eligible(df):
    df_filled = manager.neighbor_average_fill(df)
    data = manager.prepare_torch_data(df_filled)
  else:
    print("Sample at", t, "not eligible; saving empty Tensor")
    data = torch.Tensor()
  file = os.path.join(torch_save_folder, timestamp_to_pickle_name(t))
  torch.save(data, file)
  print("Saved to", file)
  print_now()


def get_missing_files() -> list[pd.Timestamp]:
  """Return list of timestamps whose Torch sample files are not present"""
  missing = []
  all_time_instants = get_time_instants()
  for t in all_time_instants:
    file = os.path.join(torch_save_folder, timestamp_to_pickle_name(t))
    if not os.path.exists(file):
      print("Missing:", t)
      missing.append(t)
  print("\nTotal missing:", len(missing), "out of", len(all_time_instants))
  return missing


if __name__ == '__main__':
  parallelism = 128
  # target_metric = 'pcie_t1'
  # target_metric = 'pcie_max_t2'; aggregator = np.nanmax
  target_metric = 'pcie_t1_delta'
  dataset = 'minimal'

  edges_file = os.path.join('resources', 'edges_distance.csv')
  weights_file = os.path.join('resources', 'weights_distance.csv')
  metric_sel_file = os.path.join('resources', f'metrics_{dataset}_dataset.csv')
  torch_save_folder = os.path.join(BASE_PATH_,
                                   f'torch_{dataset}_y={target_metric}')
  if not os.path.exists(torch_save_folder):
    raise OSError(f"Non-existent folder {torch_save_folder}")

  logging.basicConfig(level=logging.INFO)
  edge_index = torch.tensor(np.loadtxt(edges_file).T, dtype=torch.long)
  edge_weight = torch.tensor(np.loadtxt(weights_file), dtype=torch.float)
  manager = GraphDataManager(target_metric, edge_index, edge_weight,
                             metric_sel_file)

  print(f"Saving data to folder {torch_save_folder}...", flush=True)
  time.sleep(3)
  if parallelism != 1:
    # Attempt to save files in parallel (may leave hanging processes)
    with Pool(parallelism) as pool:
      pool.map(save_pickle, get_missing_files())
  else:
    # Save remaining files sequentially
    for t in get_missing_files():
      save_pickle(t)

  print("Bye!")
