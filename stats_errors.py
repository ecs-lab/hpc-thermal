import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import sys


"""Script to read training results from log files into plots

It accepts a command-line argument, i.e. it must be run as:
`python stats_errors.py EXPERIMENT_SIGNATURE`
where EXPERIMENT_SIGNATURE is the name of the log files after the
'errors_train_' / 'errors_test_' part, without the '.csv' file extension.
"""


if __name__ == '__main__':
  show_lvp = True
  experiment_suffix = sys.argv[1]
  train_log = os.path.join('logs', f'errors_train_{experiment_suffix}.csv')
  test_log = os.path.join('logs', f'errors_test_{experiment_suffix}.csv')
  is_node_model = ('_node' in experiment_suffix)
  if is_node_model:
    node_num = int(experiment_suffix.split('_node')[1].split('_')[0])
    nodes = [node_num]
  else:
    nodes = [417, 298, 85, 949]

  target_metric = experiment_suffix.split('target-')[-1].split('_rng')[0]
  print("Using target metric", target_metric)
  df = pd.read_csv(test_log, index_col='time', parse_dates=True)
  df[['ytrue', 'ypred']] = df[['ytrue', 'ypred']].astype(str)
  oneday = pd.Timedelta(days=1)
  split = lambda R : [float(r) for r in R.split('/')]
  ytrue_all = pd.DataFrame(df['ytrue'].apply(split).tolist(), index=df.index)
  ypred_all = pd.DataFrame(df['ypred'].apply(split).tolist(), index=df.index)


def get_average_losses(df: pd.DataFrame) -> dict[float]:
  return df[['train_loss','train_loss_lvp', 'eval_loss', 'eval_loss_lvp']] \
           .mean(axis=0).to_dict()


def plot_aggregate_residuals():
  """Plot aggregate (e.g. average, min/max) residuals across all nodes"""
  residuals_all = ytrue_all - ypred_all
  aggs = residuals_all.agg(['mean', 'min', 'max', 'std'], axis=1)
  means = aggs.mean(axis=0)
  ax = aggs.plot(figsize=(24, 6))
  ax.legend(labels=[f"{a} (avg: {means[a]:.3f})" for a in aggs.columns])
  ax.grid(axis='y')
  ax.set_xlim((df.index[0]-oneday, df.index[-1]+oneday))
  ax.set_xticks(pd.date_range(df.index[0], df.index[-1], freq='10D'))
  ax.set_title("Residuals distribution across nodes in test set "
              f"({target_metric})")
  fig = ax.get_figure()
  file = f'pics/residuals_{experiment_suffix}.png'
  fig.savefig(file, bbox_inches='tight', dpi=300)
  plt.close(fig)
  print("Saved to", file)


def plot_predictions(node: int, show_lvp: bool = True):
  """Plot actual vs predicted values for `node` on the whole test set"""
  figsize = (24, 6)
  ytrue = ytrue_all[node] if node in ytrue_all.columns else ytrue_all
  ypred = ypred_all[node] if node in ypred_all.columns else ypred_all
  # Plot stuff
  fig, ax = plt.subplots(figsize=figsize)
  ax.plot(ypred, label='prediction', zorder=4)
  ax.plot(ytrue, label='target', zorder=2)
  ax.grid(axis='y')
  ax.set_xlim((df.index[0]-oneday, df.index[-1]+oneday))
  ax.set_xticks(pd.date_range(df.index[0], df.index[-1], freq='10D'))
  ax.set_title(f"Predictions for node {node} ({target_metric})")
  ax.legend()
  file = f'pics/predictions_n{node}_{experiment_suffix}.png'
  fig.savefig(file, bbox_inches='tight', dpi=300)
  plt.close(fig)
  print("Saved to", file)


def plot_local_predictions(node: int, start: str, stop: str):
  """Plot actual vs predicted values for `node` on a portion of the test set"""
  figsize = (12, 4)
  t0 = pd.to_datetime(start, utc=True)
  t1 = pd.to_datetime(stop, utc=True)
  ytrue = ytrue_all[node] if node in ytrue_all.columns else ytrue_all
  ypred = ypred_all[node] if node in ypred_all.columns else ypred_all
  ytrue = ytrue.loc[t0:t1]
  ypred = ypred.loc[t0:t1]
  # Plot stuff
  fig, ax = plt.subplots(figsize=figsize)
  ax.plot(ypred, marker='.', c='#2ca02c', label='prediction', zorder=4)
  ax.plot(ytrue, marker='.', c='#d62728', label='target', zorder=2)
  ax.grid(axis='both')
  ax.set_title(f"Predictions for node {node} ({target_metric}) "
               f"in {start} ~ {stop}")
  ax.legend()
  file = (f'pics/pred_loc_n{node}_{experiment_suffix}_'
          f'from{start.replace(" ", "_")}_to__.png')  # stop.replace(" ", "_")
  fig.savefig(file, bbox_inches='tight', dpi=300)
  plt.close(fig)
  print("Saved to", file)


def plot_target_values(nodes: list[int]):
  """Plot actual values for a list of `nodes` on the whole test set"""
  fig, ax = plt.subplots(figsize=(42, 6))
  for node in nodes:
    ax.plot(ytrue_all[node], label=node)
  ax.grid(axis='y')
  t0 = ytrue_all.index[0]
  t1 = ytrue_all.index[-1]
  ax.set_xlim(t0-oneday, t1+oneday)
  ax.set_xticks(pd.date_range(t0, t1, freq='10D'))
  ax.set_title(f"Target values per node ({target_metric})")
  ax.legend()
  fig.autofmt_xdate()
  file = f'pics/target_{target_metric}.png'
  fig.savefig(file, bbox_inches='tight', dpi=300)
  plt.close(fig)
  print("Saved to", file)


def plot_local_target_values(nodes: list[int], start: str, stop: str):
  """Plot actual values for a list of `nodes` on a portion of the test set"""
  figsize = (24, 6)
  ylim = (25, 55)
  fig, ax = plt.subplots(figsize=figsize)
  t0 = pd.to_datetime(start, utc=True)
  t1 = pd.to_datetime(stop, utc=True)
  for node in nodes:
    ax.plot(ytrue_all.loc[t0:t1, node], marker='.', label=node)
  ax.grid(axis='y')
  ax.set_xlim(t0-pd.Timedelta(minutes=15), t1+pd.Timedelta(minutes=15))
  ax.set_ylim(ylim)
  ax.set_title(f"Target values ({target_metric}) per node in {start} ~ {stop}")
  ax.legend(loc='upper left')
  fig.autofmt_xdate()
  file = f'pics/target_{target_metric}_from{start}_to{stop}.png'
  fig.savefig(file, bbox_inches='tight', dpi=300)
  plt.close(fig)
  print("Saved to", file)


def plot_train_loss(ymax, show_test: bool, show_lvp: bool = True):
  """Plot evolution of the training loss during training and testing

  This function also covers the case in which training data has been shuffled:
  the timestamp is therefore not used here, since it has no significance in
  that case.
  """
  ylim = (-1, ymax)
  df_train = pd.read_csv(train_log)
  labels = ['batch train loss']
  if show_test:
    # Also extract test-set metrics
    df_test = df['train_loss']
    labels.append('sample test loss')
  else:
    df_test = None
  # Create combined DataFrame with all relevant information
  df_list = [df_train['loss'], df_test]
  if show_lvp:
    df_list.append(df['train_loss_lvp'])
    labels.append('LVP sample test loss')
  df_train_loss = pd.concat(df_list, axis=1).reset_index(drop=True)
  df_train_loss.columns = labels
  # Compute relevant quantities
  n_train_points = df_train_loss.shape[0]
  epoch_means = df_train[['epoch', 'loss']].groupby('epoch').mean()
  print("Epoch averages =", epoch_means['loss'].tolist())

  # Plot all quantities
  ax = df_train_loss.plot(figsize=(24, 6))
  # Draw epoch lines, assuming df_train is sorted by epoch then batch number
  num_epochs = df_train['epoch'].iloc[-1] + 1
  x_epochs_changes = [0]  # list of values in which epoch changes happen
  for i in range(1, num_epochs+1):
    x_epoch_change = df_train['epoch'].searchsorted(i, side='left')
    x_epochs_changes.append(x_epoch_change)
    # We need the following if-else to not get repeated legend labels
    if i == 1:
      label_h = 'epoch average loss'
      label_v = 'epoch change'
    else:
      label_h = None
      label_v = None
    # Draw horizontal line representing the epoch average loss
    ax.axhline(y=epoch_means.loc[i-1].item(),
                xmin=x_epochs_changes[i-1]/n_train_points,
                xmax=x_epochs_changes[i]  /n_train_points,
                ls='--', color='cyan', label=label_h, zorder=2)
    # Draw vertical line representing the epoch change
    if i != num_epochs:
      ax.axvline(x_epoch_change, ls='--', c='red', label=label_v)
  # Other plot goodies
  ax.margins(x=0)
  ax.set_ylim(ylim)
  ax.grid(axis='y')
  ax.set_title(f"Training loss ({target_metric})")
  ax.legend()
  # Save plot
  fig = ax.get_figure()
  file = f'pics/loss_train_{experiment_suffix}.png'
  fig.savefig(file, bbox_inches='tight', dpi=300)
  plt.close(fig)
  print("Saved to", file)


def plot_eval_loss(show_lvp: bool = True):
  """Plot values of the evaluation loss on the test set"""
  fig, ax = plt.subplots(figsize=(24, 6))
  ax.plot(df['eval_loss'],
          label=f"Model loss (avg: {df['eval_loss'].mean():.3f})")
  if show_lvp:
    ax.plot(df['eval_loss_lvp'],
            label=f"LVP loss (avg: {df['eval_loss_lvp'].mean():.3f})")
  ax.legend()
  ax.grid(axis='y')
  ax.set_xlim((df.index[0]-oneday, df.index[-1]+oneday))
  ax.set_xticks(pd.date_range(df.index[0], df.index[-1], freq='10D'))
  ax.set_title(f"Evaluation loss on test set ({target_metric})")
  file = f'pics/loss_eval_{experiment_suffix}.png'
  fig.savefig(file, bbox_inches='tight', dpi=300)
  plt.close(fig)
  print("Saved to", file)


def plot_mse_per_node(node_models_data: pd.Series | None = None):
  """Plot Mean Square Errors of individual nodes, for the model and the LVP

  This method is **only** for the case in which the LVP is identically zero.
  Node MSEs are shown in decreasing order with respect to the MSE value of the
  model. Average values for both model and LVP MSEs are also shown.
  Optionally, the MSEs for single-node data models can be provided. If so, they
  will also be displayed.
  """
  # Compute MSEs
  df_mse = pd.DataFrame(index=ytrue_all.columns, columns=['model', 'LVP'])
  df_mse['model'] = ((ytrue_all - ypred_all) ** 2).mean(axis=0)
  df_mse['LVP'] = (ytrue_all ** 2).mean(axis=0)
  # Sort MSEs and compute min, max, avg
  df_mse.sort_values('model', ascending=False, inplace=True)
  df_mse.index = df_mse.index.astype(str)
  mse_min = np.floor(df_mse.iloc[-1, 0])
  mse_max = 6.001
  mse_avg = df_mse['model'].mean()
  mse_avg_lvp = df_mse['LVP'].mean()
  # Plot MSEs and average lines
  fig, ax = plt.subplots(figsize=(10, 6))
  ax.plot(df_mse['model'], c='tab:blue', label='GNN model', zorder=4)
  ax.axhline(mse_avg, c='darkblue', ls='--', zorder=6,
                      label='GNN model average')
  ax.plot(df_mse['LVP'], c='tab:orange', label='LVP', zorder=2)
  ax.axhline(mse_avg_lvp, c='red', ls='--', zorder=6, label='LVP average')
  # Plot extra data if provided
  if node_models_data is not None:
    node_models_data.index = node_models_data.index.astype(str)
    node_models_data = node_models_data.loc[df_mse.index]
    ax.plot(node_models_data, c='tab:green', label='Ridge models', zorder=2)
    node_models_avg = node_models_data.mean()
    print(f">> {experiment_suffix}: model {mse_avg}, lvp {mse_avg_lvp}, "
          f"ridge {node_models_avg}")
    ax.axhline(node_models_avg, c='darkgreen', ls='--', zorder=6,
               label='Ridge models average')
  # Other plot goodies
  ax.set_xticks([])
  ax.set_ylim(mse_min - 0.001, mse_max + 0.001)
  ax.set_yticks(np.arange(mse_min, mse_max, 1), minor=False)
  ax.grid(axis='y', which='major', alpha=1)
  ax.set_yticks(np.arange(mse_min, mse_max, 0.5), minor=True)
  ax.grid(axis='y', which='minor', alpha=0.5)
  # ax.set_title(f"Sorted MSEs of single nodes")
  ax.legend()

  file = f'pics/mse_nodes_{experiment_suffix}.png'
  fig.savefig(file, bbox_inches='tight', dpi=300)
  plt.close(fig)
  print("Saved to", file)



if __name__ == '__main__':
  # avgs = get_average_losses(df)
  # print(avgs)
  # strg = ','.join([str(round(v, 3)) for v in avgs.values()])
  # print(f'"{experiment_suffix}",{strg}\n---')
  # plot_eval_loss(show_lvp=show_lvp)
  # if not is_node_model:
  #   plot_aggregate_residuals()
  for node in [85]:
    # plot_predictions(node, show_lvp=show_lvp)
    plot_local_predictions(node, start='2022-07-05 12:45',
                                 stop= '2022-07-06 20:00')
  # plot_train_loss(ymax=4, show_test=True, show_lvp=show_lvp)
  node_models_data = pd.read_csv('nodes_Ridge_5.local.csv',
                                 index_col='node')['mse']
  plot_mse_per_node(node_models_data)
  # plot_target_values(nodes)
  # plot_local_target_values(nodes, start='2022-09-01', stop='2022-09-03')
