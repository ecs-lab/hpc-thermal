#!/bin/bash
#SBATCH -A GrM_GRAPHHPC
#SBATCH -p boost_usr_prod
#SBATCH --time 01:00:00  # HH:MM:SS
#SBATCH -N 1             # nodes
#SBATCH --gres=gpu:1     # GPUs/node (max 4)
#SBATCH -e %j.txt
#SBATCH -o %j.txt

# Example of shell script calling the CLI for train_model.py (static model)

target_metric='pcie_t1_delta'
lvp_metric='0'
dataset='minimal'
root_folder='/PATH/TO'
samples_folder="${root_folder}/data/torch_${dataset}_y=${target_metric}"

source "${root_folder}/gmenv_py311/bin/activate"

python3 cli.py \
  --neurons 3 \
  --num-epochs 10 \
  --batch-size 20 \
  --rng-seed 42 \
  --target-metric "${target_metric}" \
  --samples-folder "${samples_folder}" \
  --eligible-log "resources/eligible_y=${target_metric}.txt" \
  --lvp-metric "${lvp_metric}" \
  --train-loss 'mse_loss' \
  --eval-loss 'MeanAbsolutePercentageError' \
  --network-type 'GCNRandomGraphStaticPredictionModel' \
  # --network-type 'NodeDenseStaticPredictionModel' \
  # --node 85
