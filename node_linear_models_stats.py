import os
import pandas as pd
import matplotlib.pyplot as plt


def plot_local_predictions(node: int, start: str, stop: str):
  """Copied from stats_errors.py"""
  figsize = (12, 4)
  t0 = pd.to_datetime(start, utc=True)
  t1 = pd.to_datetime(stop, utc=True)
  ytrue = ytrue_all[node] if node in ytrue_all.columns else ytrue_all
  ypred = ypred_all[node] if node in ypred_all.columns else ypred_all
  ytrue = ytrue.loc[t0:t1]
  ypred = ypred.loc[t0:t1]
  # Plot stuff
  fig, ax = plt.subplots(figsize=figsize)
  ax.plot(ypred, marker='.', c='#2ca02c', label='prediction', zorder=4)
  ax.plot(ytrue, marker='.', c='#d62728', label='target', zorder=2)
  ax.grid(axis='both')
  ax.set_title(f"Predictions for node {node} ({target_metric}) "
               f"in {start} ~ {stop}")
  ax.legend()
  file = (f'pics/pred_loc_n{node}_Ridge_'
          f'from{start.replace(" ", "_")}_to{stop.replace(" ", "_")}.png')
  fig.savefig(file, bbox_inches='tight', dpi=300)
  plt.close(fig)
  print("Saved to", file)


if __name__ == '__main__':
  experiment_suffix = ('GCNStaticPredictionModel_2023-08-22_10:25_layer15_'
                       'epoch10_batch20_target-pcie_t1_delta_rng42')
  train_log = os.path.join('logs', f'errors_train_{experiment_suffix}.csv')
  test_log = os.path.join('logs', f'errors_test_{experiment_suffix}.csv')
  node = 85

  target_metric = experiment_suffix.split('target-')[-1].split('_rng')[0]
  print("Using target metric", target_metric)
  df = pd.read_csv(test_log, index_col='time', parse_dates=True)
  df[['ytrue', 'ypred']] = df[['ytrue', 'ypred']].astype(str)
  split = lambda R : [float(r) for r in R.split('/')]
  print("Reading data from log")
  ytrue_all = pd.DataFrame(df['ytrue'].apply(split).tolist(), index=df.index)
  print("Reading data from Ridge csv")
  ypred_all = pd.read_csv('nodes_Ridge_5_leo.csv', parse_dates=True,
                          index_col='time', usecols=['time', str(node)])
  print("Done")

  plot_local_predictions(node=85, start='2022-07-05 12:45',
                                  stop= '2022-07-06 20:00')
